### 0.9 - 10/04/14

* Added support for both versioned and unversioned .xp formats

* Adjusted namespace from Varnerin.RexTools to RexTools

* Made RexReader implement IDisposable

### 0.6 - 5/20/13

* Moved to .NET 3.0, removing LINQ and some built-in stream helper functions

* Added this fancy changelog
